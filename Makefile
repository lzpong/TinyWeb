PWD=$(shell pwd)

#INCS=-I../libuv/include/
LIBEDIT_DIR=
DEBUG=-g -ggdb -gstabs+
BASE_FLAGS=$(INCS) $(DEBUG) $(LIBEDIT_DIR) -lpthread -ldl -fPIE #-fPIC
LDFLAGS=-L.

CC_CFLAGS=$(BASE_FLAGS) #-Wl,-rpath=/usr/local/lib/
CXX_CFLAGS=$(BASE_FLAGS) #-Wl,-rpath=/usr/local/lib/

CC=gcc
CXX=g++

#OBJS=Main.o tinyweb.o tools.o
#SRC=Main.c tinyweb.c tools.c
#HEADERS=tinyweb.h tools.h

#SOLINK=-shared -Xlinker -x


all: tinyweb


tinyweb: tools.o tinyweb.o Main.o  libuv.a
	$(CC) $(CFLAGS) -o tinyweb Main.o tinyweb.o tools.o -lrt libuv.a  $(CC_CFLAGS)
	@echo make tinyweb done.



libuv.a:
	cp /usr/local/lib/libuv.a ./


%.o: %.c $(HEADERS)
	$(CC) $(CFLAGS) -c $< -o $@  $(CC_CFLAGS)

%.o: %.cpp $(HEADERS)
	$(CXX) $(CXXFLAGS) -c $< -o $@  $(CXX_CFLAGS)



clean:
	rm -f *.o
